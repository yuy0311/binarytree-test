/**
 * Created by Yang on 11/07/2014.
 */
public class DecoratedNode
{
	Node node;
	int level;

	public DecoratedNode(Node node,int level)
	{
		this.node = node;
		this.level = level;
	}
}
