import java.sql.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Yang on 11/07/2014.
 */
public class Run
{
	public static void main(String [] args)
	{
		Run r = new Run();
		/*
		//init a leaf node first
		Node two = new Node(2,null,null);
		Node five = new Node(5,null,null);
		Node nine = new Node(9,null,null);
		Node four = new Node(4,two,null);
		Node one = new Node(1,four,five);
		Node three = new Node(3,nine,null);
		Node fiveroot = new Node(5,three,one);
		*/
		Node two = new Node(2,null,null);
		Node five = new Node(5,two,two);
		Node nine = new Node(9,five,five);
		Node four = new Node(4,nine,nine);
		Node one = new Node(1,four,four);
		Node three = new Node(3,one,one);
		Node fiveroot = new Node(5,three,three);
		r.PrintTree(fiveroot);
	}

	public void PrintTree(Node n)
	{
		int startlevel = 0;
		int firsObject = 0;
		List<DecoratedNode> queue = new ArrayList<DecoratedNode>();
		DecoratedNode dnode = new DecoratedNode(n,startlevel);
		queue.add(dnode);
		while(queue.size() > 0)
		{
			DecoratedNode currentvistnode = queue.get(firsObject);
			DecoratedNode nextvistnode = null;
			if(queue.size()>1)
				nextvistnode = queue.get(firsObject+1);
			this.outputValue(currentvistnode,nextvistnode);
			if(currentvistnode.node.left != null)
			{
				queue.add(new DecoratedNode(currentvistnode.node.left, currentvistnode.level+1));
			}

			if(currentvistnode.node.right != null)
			{
				queue.add(new DecoratedNode(currentvistnode.node.right, currentvistnode.level+1));
			}
		   queue.remove(0);
		}
	}

	public void outputValue(DecoratedNode currentvistnode, DecoratedNode nextvistnode)
	{
		int level = currentvistnode.level;
		System.out.print(currentvistnode.node.value);
		if(nextvistnode!= null && nextvistnode.level == level)
		{
			System.out.print(",");
		}
		else
		{
			System.out.print("\n");
		}
	}
}

